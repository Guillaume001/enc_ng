use clap_complete::{generate_to, shells::Bash};
use std::env;
use std::fs;
use std::io::Error;
use std::io::{Read, Write};

include!("src/cli/cli.rs");

fn replace_word(filename: String, from: &str, to: &str) -> Result<(), Error> {
    let mut file = fs::File::open(&filename)?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let new_contents = contents.replace(from, to);

    let mut file = fs::File::create(filename)?;
    file.write_all(new_contents.as_bytes())?;

    Ok(())
}

fn main() -> Result<(), Error> {
    println!("cargo:rerun-if-changed=src/cli.rs");

    let outdir = match env::var_os("OUT_DIR") {
        None => panic!("OUT_DIR not set"),
        Some(outdir) => outdir,
    };

    let mut cmd = build_cli();
    generate_to(Bash, &mut cmd, "nccli", outdir.clone())?;

    // replace enc_ng with nccli in the generated file nccli.bash
    replace_word(
        outdir.to_str().unwrap().to_owned() + "/nccli.bash",
        "enc_ng",
        "nccli",
    )?;

    // move nccli.bash to target/
    std::fs::rename(
        outdir.to_str().unwrap().to_owned() + "/nccli.bash",
        "../target/nccli.bash",
    )?;
    Ok(())
}
