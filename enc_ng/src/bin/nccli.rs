use anyhow::{Error, Result};
use enc_ng::cli::node::node_subcommand;
use enc_ng::cli::parameter::parameter_subcommand;
use enc_ng::cli::role::role_subcommand;
use enc_ng::Action;
use enc_ng_lib::{read_core_config, Config};
use std::env;
use std::path::PathBuf;

include!("../cli/cli.rs");

fn main() -> Result<()> {
    env_logger::Builder::from_default_env().init();

    let mut command = build_cli();

    command = enc_ng_lib::add_default_arguments(command);
    let matches = command.get_matches();

    let config_path = matches
        .get_one::<PathBuf>("config")
        .unwrap_or_else(|| panic!("Cannot get the path of configuration file"));
    let config = read_core_config(config_path)?;

    if let Some(matches) = matches.subcommand_matches("add") {
        add_process(matches, &config)?;
    } else if let Some(matches) = matches.subcommand_matches("delete") {
        delete_process(matches, &config)?;
    } else if let Some(matches) = matches.subcommand_matches("edit") {
        edit_process(matches, &config)?;
    } else if let Some(matches) = matches.subcommand_matches("list") {
        list_process(matches, &config)?;
    } else if let Some(matches) = matches.subcommand_matches("show") {
        show_process(matches, &config)?;
    }

    Ok(())
}

fn add_process(matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    if let Some(matches) = matches.subcommand_matches("node") {
        node_subcommand(Action::Add, matches, config)?;
    }
    Ok(())
}
fn delete_process(matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    if let Some(matches) = matches.subcommand_matches("node") {
        node_subcommand(Action::Delete, matches, config)?;
    }
    Ok(())
}
fn edit_process(matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    if let Some(matches) = matches.subcommand_matches("node") {
        node_subcommand(Action::Edit, matches, config)?;
    }
    Ok(())
}

fn list_process(matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    if let Some(matches) = matches.subcommand_matches("nodes") {
        node_subcommand(Action::List, matches, config)?;
    } else if let Some(matches) = matches.subcommand_matches("parameters") {
        parameter_subcommand(Action::List, matches, config)?;
    } else if let Some(matches) = matches.subcommand_matches("roles") {
        role_subcommand(Action::List, matches, config)?;
    }
    Ok(())
}

fn show_process(matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    if let Some(matches) = matches.subcommand_matches("node") {
        node_subcommand(Action::Show, matches, config)?;
    } else if let Some(matches) = matches.subcommand_matches("parameter") {
        parameter_subcommand(Action::Show, matches, config)?;
    }
    Ok(())
}
