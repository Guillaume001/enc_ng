use std::path::PathBuf;

use anyhow::Result;
use clap::{arg, command};
use enc_ng_lib::read_core_config;

fn main() -> Result<()> {
    env_logger::Builder::from_default_env().init();

    let mut command = command!()
        .about("ENC NG Puppet: binary that meets the requirements of the Puppet server")
        .arg(
            arg!(<NODE_NAME>)
                .help("Node name or FQDN (Ex: node1.example.org)")
                .required(true)
                .index(1),
        );

    command = enc_ng_lib::add_default_arguments(command);
    let matches = command.get_matches();

    let config_path = matches
        .get_one::<PathBuf>("config")
        .unwrap_or_else(|| panic!("Cannot get the path of configuration file"));
    let config = read_core_config(config_path)?;

    let node_name = matches
        .get_one::<String>("NODE_NAME")
        .cloned()
        .expect("Node name is mandatory");

    let result = enc_ng_lib::get_enc_for_puppet(&config, node_name)?;
    print!("{result}");
    Ok(())
}
