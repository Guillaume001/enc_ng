use anyhow::{anyhow, bail, Error, Result};
use enc_ng_lib::{Config, ParameterEntry};

use crate::Action;

pub fn parameter_subcommand(
    action: Action,
    matches: &clap::ArgMatches,
    config: &Config,
) -> Result<(), Error> {
    if action == Action::Show {
        show_process(matches, config)?;
    } else if action == Action::List {
        list_process(matches, config)?;
    }
    Ok(())
}

fn list_process(list_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    // get format from arguments
    let format = list_matches
        .get_one::<String>("format")
        .cloned()
        .unwrap_or_else(|| "humain".to_string());

    match format.as_str() {
        "humain" => render_parameters_humain(&config.parameters),
        "raw" => render_parameters_raw(&config.parameters),
        "json" => render_parameters_json(&config.parameters),
        "yaml" => render_parameters_yaml(&config.parameters),
        "toml" => render_parameters_toml(&config.parameters),
        _ => bail!("Unknown format '{format}'"),
    }
    Ok(())
}

fn render_parameters_humain(parameters: &Vec<ParameterEntry>) {
    if !parameters.is_empty() {
        println!("Available parameters:");
        parameters.iter().for_each(|parameter| {
            println!("\t- {}", parameter.name);
        });
    } else {
        println!("No parameters available");
    }
}

fn render_parameters_raw(parameters: &[ParameterEntry]) {
    parameters
        .iter()
        .for_each(|parameter| println!("{}", parameter.name));
}

fn render_parameters_json(parameters: &[ParameterEntry]) {
    let json = serde_json::to_string_pretty(parameters).unwrap();
    println!("{json}");
}

fn render_parameters_yaml(parameters: &[ParameterEntry]) {
    let yaml = serde_yaml::to_string(parameters).unwrap();
    println!("{yaml}");
}

fn render_parameters_toml(parameters: &[ParameterEntry]) {
    let toml = toml::to_string(parameters).unwrap();
    println!("{toml}");
}

fn show_process(show_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    let parameter_name = show_matches
        .get_one::<String>("PARAMETER_NAME")
        .cloned()
        .expect("Parameter name is mandatory");

    let param_entry = config
        .parameters
        .iter()
        .find(|&p| p.name == *parameter_name)
        .ok_or_else(|| anyhow!("Parameter '{parameter_name}' not found"))?;

    println!("Parameter\t: {}", &param_entry.name);
    if param_entry.description.is_some() {
        println!(
            "Description\t: {}",
            &param_entry.description.clone().unwrap_or_default()
        );
    }
    println!("Required\t: {}", &param_entry.required.unwrap_or_default());
    if param_entry.allowed_values.is_some() {
        println!(
            "Allowed values\t: {}",
            &param_entry
                .allowed_values
                .clone()
                .unwrap_or_default()
                .join(", ")
        );
    }
    if param_entry.format.is_some() {
        println!(
            "Format\t\t: {}",
            &param_entry.format.clone().unwrap_or_default()
        );
    }
    Ok(())
}
