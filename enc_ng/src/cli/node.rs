use anyhow::{anyhow, bail, Error, Result};
use cli_table::{print_stdout, TableStruct};
use enc_ng_lib::{Config, Node};
use std::{
    collections::HashMap,
    io::{self, Write},
};

use crate::Action;

#[macro_export]
macro_rules! get_hostname_argument {
    ( $matches: expr ) => {
        $matches
            .get_one::<String>("HOSTNAME")
            .cloned()
            .expect("Hostname is mandatory")
    };
}

pub fn node_subcommand(
    action: Action,
    matches: &clap::ArgMatches,
    config: &Config,
) -> Result<(), Error> {
    if action == Action::Add {
        add_process(matches, config)?;
    } else if action == Action::Show {
        show_process(matches, config)?;
    } else if action == Action::Edit {
        edit_process(matches, config)?;
    } else if action == Action::Delete {
        delete_process(matches, config)?;
    } else if action == Action::List {
        list_process(matches, config)?;
    }
    Ok(())
}

fn add_process(add_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    enc_ng_lib::add_node(
        config,
        get_hostname_argument!(add_matches),
        add_matches.get_one::<String>("role").cloned().unwrap(),
        Some(parse_parameters(add_matches)),
    )?;
    Ok(())
}

fn parse_parameters(matches: &clap::ArgMatches) -> HashMap<String, String> {
    let arg_parameters = matches
        .get_many::<String>("parameter")
        .unwrap_or_default()
        .map(|v| v.as_str())
        .collect::<Vec<_>>();
    let mut parameters = HashMap::new();
    for arg_parameter in arg_parameters {
        let (key, value) = arg_parameter.split_once('=').unwrap();
        parameters.insert(key.to_string(), value.to_string());
    }
    parameters
}

fn show_process(show_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    let yaml_output = enc_ng_lib::get_enc_for_puppet(config, get_hostname_argument!(show_matches))?;
    println!("{yaml_output}");
    Ok(())
}

fn display_node_information(node: &Node) {
    println!("Node information:");
    println!("  Name: {}", node.hostname);
    println!("  Classes:");
    for c in &node.classes {
        println!("    - {c}");
    }
    if let Some(params) = &node.parameters {
        println!("  Parameters:");
        for (key, value) in params {
            println!("    {key}: {value}");
        }
    }
}

fn delete_process(delete_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    let node_name = get_hostname_argument!(delete_matches);
    let node = enc_ng_lib::find_node(config, node_name.clone())
        .ok_or_else(|| anyhow!("Node not found"))?;
    // Show node information before deleting
    display_node_information(&node);
    // prompt for confirmation
    if !delete_matches.get_flag("yes") {
        let mut input = String::new();
        print!("\nAre you sure you want to delete node '{node_name}'? [y/N] ");
        io::stdout().flush()?;
        io::stdin().read_line(&mut input)?;
        if input.trim() != "y" {
            bail!("Aborted");
        }
    }
    enc_ng_lib::delete_node(config, get_hostname_argument!(delete_matches))?;
    Ok(())
}

fn edit_process(modify_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    enc_ng_lib::modify_node(
        config,
        get_hostname_argument!(modify_matches),
        modify_matches.get_one::<String>("role").cloned(),
        Some(parse_parameters(modify_matches)),
    )?;
    Ok(())
}

fn list_process(list_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    // get filter pattern
    let filter = list_matches.get_one::<String>("filter").cloned();

    let mut nodes = enc_ng_lib::get_all_nodes(config, filter)?;
    // Sort nodes by hostname
    nodes.sort_by(|a, b| a.hostname.cmp(&b.hostname));

    // get format from arguments
    let format = list_matches
        .get_one::<String>("format")
        .cloned()
        .unwrap_or_else(|| "table".to_string());

    match format.as_str() {
        "table" => render_nodes_table(&nodes, config)?,
        "json" => render_nodes_json(&nodes),
        "yaml" => render_nodes_yaml(&nodes),
        "toml" => render_nodes_toml(&nodes),
        "csv" => match render_nodes_csv(&nodes, config) {
            Ok(csv) => println!("{csv}"),
            Err(e) => bail!("Error rendering CSV: {e}"),
        },
        _ => bail!("Unknown format '{format}'"),
    }

    Ok(())
}

fn render_nodes_json(nodes: &[Node]) {
    let json = serde_json::to_string_pretty(nodes).unwrap();
    println!("{json}");
}

fn render_nodes_yaml(nodes: &[Node]) {
    let yaml = serde_yaml::to_string(nodes).unwrap();
    println!("{yaml}");
}

fn render_nodes_toml(nodes: &[Node]) {
    let toml = toml::to_string(nodes).unwrap();
    println!("{toml}");
}

fn render_nodes_csv(nodes: &[Node], config: &Config) -> Result<String, Error> {
    let mut wtr = csv::WriterBuilder::new()
        .quote_style(csv::QuoteStyle::Always)
        .from_writer(vec![]);

    let mut titles = vec!["node", "classes"];
    // add config parameters as title
    for parameter in config.parameters.iter() {
        titles.push(&parameter.name);
    }

    wtr.write_record(titles).unwrap();

    for node in nodes {
        let mut values = vec![node.hostname.clone(), node.classes.join(", ")];
        // add config parameters as value
        for parameter in config.parameters.iter() {
            let value = node
                .parameters
                .as_ref()
                .and_then(|p| p.get(&parameter.name));
            values.push(value.cloned().unwrap_or_default());
        }
        wtr.write_record(values).unwrap();
    }

    wtr.flush().unwrap();
    Ok(String::from_utf8(wtr.into_inner()?)?)
}

fn render_nodes_table(nodes: &[Node], config: &Config) -> Result<(), Error> {
    let csv = render_nodes_csv(nodes, config)?;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(csv.as_bytes());
    let table = TableStruct::try_from(&mut rdr).unwrap();
    print_stdout(table)?;
    Ok(())
}
