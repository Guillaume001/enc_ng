use clap::{arg, command, ArgAction, Command};

#[macro_export]
macro_rules! add_hostname_argument {
    ( ) => {
        arg!([HOSTNAME] "Name or FQDN (Ex: node1.example.org)")
            .required(true)
            .num_args(1)
    };
}

#[macro_export]
macro_rules! add_role_argument {
    ( $required: expr ) => {
        arg!(-r --role <ROLE> "Role (Ex: wiki, ntp, dns)")
            .required($required)
            .num_args(1)
    };
}

#[macro_export]
macro_rules! add_parameter_argument {
    ( ) => {
        arg!(-p --parameter <PARAMETER> "Parameters (Ex: site=france)")
                .action(ArgAction::Append)
                .num_args(1)
    };
}

pub fn build_cli() -> Command {
    command!()
        .about("NCCLI (Node Classifier Command Line Interface)")
        .subcommand_required(true)
        .arg_required_else_help(true)
        .infer_subcommands(true)
        .propagate_version(true)
        .subcommand(
            Command::new("add").about("Add new item (Ex: node)").subcommand(
                Command::new("node")
                    .about("Add new node")
                    .arg_required_else_help(true)
                    .arg(add_hostname_argument!())
                    .arg(add_role_argument!(true))
                    .arg(add_parameter_argument!()),
            ),
        )
        .subcommand(
            Command::new("delete").about("Delete an item (Ex: node)").subcommand(
                Command::new("node")
                    .about("Delete node")
                    .arg_required_else_help(true)
                    .arg(add_hostname_argument!())
                    .arg(
                        arg!(-y --yes "Force delete without confirmation")
                            .action(ArgAction::SetTrue),
                    ),
            ),
        )
        .subcommand(
            Command::new("edit").about("Edit an item (Ex: node)").subcommand(
                Command::new("node")
                    .about("Edit node")
                    .arg_required_else_help(true)
                    .arg(add_hostname_argument!())
                    .arg(add_role_argument!(false))
                    .arg(add_parameter_argument!()),
            ),
        )
        .subcommand(
            Command::new("list")
                .about("Show all available items (Ex: nodes, parameters, roles)")
                .subcommand(
                    Command::new("nodes").about("List enrolled nodes").arg(
                        arg!(-f --format <FORMAT> "Output format")
                            .value_parser(["table", "json", "yaml", "toml", "csv"])
                            .default_value("table"),
                    ).arg(
                        arg!(-F --filter <PATTERN> "Filter with regex")
                            .num_args(1),
                    ),
                )
                .subcommand(
                    Command::new("roles").about("List available roles").arg(
                        arg!(-f --format <FORMAT> "Output format (Ex: humain, raw, json, yaml)")
                            .value_parser(["humain", "raw", "json", "yaml"])
                            .default_value("humain"),
                    ).arg(
                        arg!(-p --prefix "Add role prefix to output")
                            .action(ArgAction::SetTrue),
                    )
                )
                .subcommand(
                    Command::new("parameters").about("List available parameters").arg(
                        arg!(-f --format <FORMAT> "Output format (Ex: humain, raw, json, yaml, toml)")
                            .value_parser(["humain", "raw", "json", "yaml", "toml"])
                            .default_value("humain"),
                    )
                ),
        )
        .subcommand(
            Command::new("show")
                .about("Show information about an item (Ex: node, parameter)")
                .subcommand(
                    Command::new("node")
                        .about("Show node")
                        .arg_required_else_help(true)
                        .arg(add_hostname_argument!()),
                )
                .subcommand(
                    Command::new("parameter").about("Show information of parameter").arg(
                        arg!([PARAMETER_NAME] "Name of parameter")
                            .required(true)
                            .num_args(1),
                    )
                ),
        )
}

#[test]
fn verify_cli() {
    build_cli().debug_assert();
}
