use anyhow::{bail, Error, Result};
use enc_ng_lib::Config;

use crate::Action;

pub fn role_subcommand(
    action: Action,
    matches: &clap::ArgMatches,
    config: &Config,
) -> Result<(), Error> {
    if action == Action::List {
        list_process(matches, config)?;
    }
    Ok(())
}

// Show list of available role in configuration
fn list_process(list_matches: &clap::ArgMatches, config: &Config) -> Result<(), Error> {
    // get format from arguments
    let format = list_matches
        .get_one::<String>("format")
        .cloned()
        .unwrap_or_else(|| "humain".to_string());

    // get prefix argument
    let with_prefix = list_matches.get_flag("prefix");

    let mut roles = vec![];
    if with_prefix {
        if let Some(role_available) = &config.role.available {
            role_available.iter().for_each(|role| {
                roles.push(format!("{}{}", config.role.prefix, role));
            });
        }
    } else {
        roles = config.role.available.clone().unwrap_or_default();
    }

    match format.as_str() {
        "humain" => render_roles_humain(&roles),
        "raw" => render_roles_raw(&roles),
        "json" => render_roles_json(&roles),
        "yaml" => render_roles_yaml(&roles),
        "toml" => render_roles_toml(&roles),
        _ => bail!("Unknown format '{format}'"),
    }
    Ok(())
}

fn render_roles_humain(roles: &[String]) {
    if !roles.is_empty() {
        println!("Available roles:");
        roles.iter().for_each(|role| println!("\t- {role}"));
    } else {
        println!("All roles are available");
    }
}

fn render_roles_raw(roles: &[String]) {
    roles.iter().for_each(|role| println!("{role}"));
}

fn render_roles_json(roles: &[String]) {
    let json = serde_json::to_string_pretty(roles).unwrap();
    println!("{json}");
}

fn render_roles_yaml(roles: &[String]) {
    let yaml = serde_yaml::to_string(roles).unwrap();
    println!("{yaml}");
}

fn render_roles_toml(roles: &[String]) {
    let toml = toml::to_string(roles).unwrap();
    println!("{toml}");
}
