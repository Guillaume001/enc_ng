pub mod cli {
    pub mod node;
    pub mod parameter;
    pub mod role;
}

// Action enum used in nccli
#[derive(PartialEq, Eq)]
pub enum Action {
    Add,
    Delete,
    Edit,
    List,
    Show,
}
