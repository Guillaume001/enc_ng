# ENC-NG

Are you using Puppet? Do you need an external node classifier? ENC-NG is for you!

## Building

    $ cargo build --release

## Development environment

### Setup

Copy the configuration file:

    $ cp enc_ng/config/core.toml.local.example enc_ng/config/core.toml.local
    $ cp enc_ng/database.yaml enc_ng/database.yaml.local

### Run nccli binary in development environment

    # Show usage
    $ cargo run --bin nccli -- --help

    # Show list of nodes
    $ cargo run --bin nccli -- --config enc_ng/config/core.toml.local list nodes


## Quick start

### Show usage of binaries

    $ nccli --help
    NCCLI (Node Classifier Command Line Interface)

    Usage: nccli [OPTIONS] <COMMAND>

    Commands:
    add     Add new item (Ex: node)
    delete  Delete an item (Ex: node)
    edit    Edit an item (Ex: node)
    list    Show all available items (Ex: nodes, parameters, roles)
    show    Show information about an item (Ex: node, parameter)
    help    Print this message or the help of the given subcommand(s)

    Options:
    -c, --config <FILE>  Sets a custom config file [default: /etc/enc-ng/core.toml]
    -h, --help           Print help
    -V, --version        Print version
---
    $ enc-puppet --help
    ENC NG Puppet: binary that meets the requirements of the Puppet server

    Usage: enc_puppet [OPTIONS] <NODE_NAME>

    Arguments:
    <NODE_NAME>  Node name or FQDN (Ex: node1.example.org)

    Options:
    -c, --config <FILE>  Sets a custom config file [default: /etc/enc-ng/core.toml]
    -h, --help           Print help
    -V, --version        Print version

### Show role available

    $ nccli list roles
    Available roles:
            - wiki
            - web
            - mail
            - dns
            - database
            - proxy

### Show parameter available

    $ nccli list parameters
    Available parameters:
            - site
            - comment
            - asset_tag
            - system_outsourcing
            - system_owner
            - application_outsourcing
            - application_owner

### Show information about parameter

    $ nccli show parameter site
    Parameter       : site
    Description     : The site where the machine is located physically
    Required        : true
    Allowed values  : france

### Add a new node

    $ nccli add node node-a.example.org \
        --role wiki \
        --parameter site=france \
        --parameter comment="Wiki server in France"

### Show the information of a node

    $ nccli show node node-a.example.org
    ---
    classes:
        - role::wiki
    parameters:
        site: france
        role: wiki
        comment: Wiki server in France
    environment: production

### List nodes

    $ nccli list nodes
    +--------------------+------------+--------+-----------------------+-----------+--------------------+--------------+-------------------------+-------------------+
    | node               | classes    | site   | comment               | asset_tag | system_outsourcing | system_owner | application_outsourcing | application_owner |
    +--------------------+------------+--------+-----------------------+-----------+--------------------+--------------+-------------------------+-------------------+
    | node-a.example.org | role::wiki | france | Wiki server in France |           |                    |              |                         |                   |
    +--------------------+------------+--------+-----------------------+-----------+--------------------+--------------+-------------------------+-------------------+
---
    $ nccli list nodes --format json
    [
        {
            "node": "node-a.example.org",
            "classes": [
                "role::wiki"
            ],
            "site": "france",
            "comment": "Wiki server in France",
            "asset_tag": "",
            "system_outsourcing": "",
            "system_owner": "",
            "application_outsourcing": "",
            "application_owner": ""
        }
    ]
---
    $ nccli list nodes --format csv
    "node","classes","site","comment","asset_tag","system_outsourcing","system_owner","application_outsourcing","application_owner"
    "node-a.example.org","role::wiki","france","Wiki server in France","","","","",""

### Edit a node

    $ nccli edit node node-a.example.org \
        --role database \
        --parameter comment="New database server"

    $ nccli show node node-a.example.org
    ---
    classes:
        - role::database
    parameters:
        site: france
        role: database
        comment: New database server
    environment: production

### Delete node

    $ nccli delete node node-a.example.org
    Node information:
        Name: node-a.example.org
        Classes:
            - role::wiki
        Parameters:
            site: france
            comment: Wiki server in France

    Are you sure you want to delete node 'node-a.example.org'? [y/N]

## Usage

When you install this projet, you have two binaries `nccli` and `enc-puppet`.

    $ nccli --help
    $ enc-puppet --help

In Puppet server you may use the following configuration:

    [master]
      node_terminus = exec
      external_nodes = /usr/bin/enc-puppet

## Packing

You can build your own RPM file to install where you want.

    $ cargo install cargo-generate-rpm
    $ cargo build --release
    $ cargo generate-rpm -p enc_ng

RPM file is located in ./target/generate-rpm/