use anyhow::{anyhow, bail, Context, Result};
use clap::{arg, value_parser, Command, ValueHint};
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashMap,
    error::Error,
    fs::File,
    io::{Read, Write},
    path::{Path, PathBuf},
};

const DEFAULT_CONFIG: &str = "/etc/enc-ng/core.toml";

/// Database
#[derive(Debug, Serialize, Deserialize)]
pub struct Database {
    pub version: String,
    pub nodes: Vec<Node>,
}

/// Node
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct Node {
    pub hostname: String,
    pub classes: Vec<String>,
    pub parameters: Option<HashMap<String, String>>,
}

/// DatabaseConfig
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct DatabaseConfig {
    pub backend_file: PathBuf,
}

/// RoleConfig
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct RoleConfig {
    pub prefix: String,
    pub add_role_in_parameters: Option<bool>,
    pub set_prefix_in_parameter: Option<bool>,
    pub available: Option<Vec<String>>,
}

/// EnvironmentConfig
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct EnvironmentConfig {
    pub default: Option<String>,
}

/// ParameterEntry
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ParameterEntry {
    pub name: String,
    pub required: Option<bool>,
    pub allowed_values: Option<Vec<String>>,
    pub format: Option<String>,
    pub description: Option<String>,
}

/// Configuration
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Config {
    pub database: DatabaseConfig,
    pub role: RoleConfig,
    pub environment: EnvironmentConfig,
    pub parameters: Vec<ParameterEntry>,
}

/// ENC
#[derive(Debug, Serialize, Deserialize)]
pub struct ENC {
    pub classes: Vec<String>,
    pub parameters: Option<HashMap<String, String>>,
    pub environment: Option<String>,
}

/// Parse a single key-value pair
pub fn parse_key_val<T, U>(s: &str) -> Result<(T, U), Box<dyn Error>>
where
    T: std::str::FromStr,
    T::Err: Error + 'static,
    U: std::str::FromStr,
    U::Err: Error + 'static,
{
    let pos = s
        .find('=')
        .ok_or_else(|| format!("invalid KEY=value: no `=` found in `{s}`"))?;
    Ok((s[..pos].parse()?, s[pos + 1..].parse()?))
}

pub fn add_default_arguments(command: Command) -> Command {
    command.arg(
        arg!(
            -c --config <FILE> "Sets a custom config file"
        )
        .default_value(DEFAULT_CONFIG)
        .required(false)
        .global(true)
        .value_hint(ValueHint::FilePath)
        .value_parser(value_parser!(PathBuf)),
    )
}

pub fn read_core_config(config_file: &Path) -> Result<Box<Config>> {
    let mut content = String::new();
    File::open(config_file)
        .with_context(|| format!("Failed to open '{}'", config_file.display()))?
        .read_to_string(&mut content)
        .with_context(|| "Failed to read the content of configuration file".to_string())?;
    let config = toml::from_str(&content)
        .map_err(|err| anyhow!("{}", err))
        .context("Failed to load config file")?;
    Ok(Box::new(config))
}

pub fn add_node(
    config: &Config,
    node_name: String,
    role: String,
    params: Option<HashMap<String, String>>,
) -> Result<()> {
    let mut database = get_database(config)?;
    // Check if the node already exists
    if find_node(config, node_name.clone()).is_some() {
        bail!("Node '{}' already exists", node_name);
    }

    // Check if the role is valid
    if !is_role_valid(config, role.clone()) {
        bail!("Role '{}' is not valid", role);
    }

    // Validate parameters
    let mut new_params = params.clone();
    if let Some(params) = &params {
        // Remove entry in hashmap if value is empty
        new_params = Some(
            params
                .iter()
                .filter(|(_, v)| !v.is_empty())
                .map(|(k, v)| (k.clone(), v.clone()))
                .collect(),
        );
        parameters_validation(config, params)?;
    }

    let new_node = Node {
        hostname: node_name,
        classes: vec![role_to_class(config, role)],
        parameters: new_params,
    };
    database.nodes.push(new_node);
    write_database(config, database)?;
    Ok(())
}

fn is_role_valid(config: &Config, role: String) -> bool {
    match &config.role.available {
        Some(available_roles) => available_roles.contains(&role),
        None => true,
    }
}

pub fn find_node(config: &Config, node_name: String) -> Option<Node> {
    let database = get_database(config).unwrap();
    let node_db = database.nodes.iter().find(|&n| n.hostname == node_name);
    let node = match node_db {
        Some(node) => node,
        None => return None,
    };
    Some(node.clone())
}

pub fn parameters_validation(config: &Config, params: &HashMap<String, String>) -> Result<()> {
    // Verify if all required parameters are present
    for param_entry in &config.parameters {
        if param_entry.required.unwrap_or(false) && !params.contains_key(&param_entry.name) {
            bail!(
                "Parameter '{}' is required but not present in the request",
                param_entry.name
            );
        }
    }

    // Verify if all parameters are valid
    for (key, value) in params {
        let param_entry = config
            .parameters
            .iter()
            .find(|&p| p.name == *key)
            .ok_or_else(|| anyhow!("Parameter '{}' not found", key))?;

        // Skip if no allowed_values are defined and no format is specified
        if param_entry.allowed_values.is_none() && param_entry.format.is_none() {
            continue;
        }

        if let Some(format) = &param_entry.format {
            // Skip if the value is empty to allow deleting this parameter
            if value.is_empty() {
                continue;
            }
            let format = Regex::new(format).unwrap();
            if !format.is_match(value) {
                bail!(
                    "Parameter '{}' has an invalid value '{}'. Expected format: {}",
                    key,
                    value,
                    format
                );
            }
            continue;
        }

        let values = param_entry.allowed_values.as_ref().unwrap();
        if !values.is_empty() && !values.contains(&value.to_string()) {
            bail!(
                "Parameter '{}' has an invalid value '{}'. Valid values are: {:?}",
                key,
                value.to_string(),
                values
            );
        }
    }
    Ok(())
}

fn write_database(config: &Config, database: Database) -> Result<()> {
    let database_file = &config.database.backend_file.clone();
    let yaml = serde_yaml::to_string(&database)?;
    File::create(database_file)
        .with_context(|| format!("Failed to open '{}'", database_file.display()))?
        .write_all(yaml.as_bytes())
        .with_context(|| "Failed to write the content of database file".to_string())?;
    Ok(())
}

pub fn get_enc_for_puppet(config: &Config, node_name: String) -> Result<String> {
    let node = find_node(config, node_name).ok_or_else(|| anyhow!("Node not found"))?;
    let enc = ENC {
        classes: node.classes.clone(),
        parameters: node.parameters,
        environment: config.environment.default.clone(),
    };
    let mut yaml = String::new();
    yaml.push_str("---\n");
    yaml.push_str("classes:\n");

    // iterate over classes and put new line only if there is a next class
    let mut classes_sorted = enc.classes.clone();
    classes_sorted.sort();
    for (i, class) in classes_sorted.iter().enumerate() {
        if i == enc.classes.len() - 1 {
            yaml.push_str(&format!("  - {class}"));
        } else {
            yaml.push_str(&format!("  - {class}\n"));
        }
    }

    let mut params: HashMap<String, String> = enc.parameters.unwrap_or_default();

    // add role as parameter if enabled
    if config.role.add_role_in_parameters.unwrap_or(false) {
        // substring to remove the prefix role from the class name
        let mut role = node.classes[0].clone();
        if !config.role.set_prefix_in_parameter.unwrap_or(true) {
            role = role.replacen(&config.role.prefix, "", 1);
        }
        params.insert("role".to_string(), role);
    }

    // iterate over parameters and put new line only if there is a next parameter
    if !params.is_empty() {
        let mut params_sorted: Vec<(&String, &String)> = params.iter().collect();
        params_sorted.sort_by(|a, b| a.0.cmp(b.0));
        yaml.push_str("\nparameters:\n");
        for (i, (key, value)) in params_sorted.iter().enumerate() {
            if i == params_sorted.len() - 1 {
                yaml.push_str(&format!("  {key}: {value}"));
            } else {
                yaml.push_str(&format!("  {key}: {value}\n"));
            }
        }
    }

    // add environment if defined
    if let Some(environment) = enc.environment {
        yaml.push_str(&format!("\nenvironment: {environment}"));
    }
    Ok(yaml)
}

fn get_database(config: &Config) -> Result<Database> {
    let database_file = config.database.backend_file.clone();
    let mut content = String::new();
    File::open(&database_file)
        .with_context(|| format!("Failed to open '{}'", database_file.display()))?
        .read_to_string(&mut content)
        .with_context(|| "Failed to read the content of database file".to_string())?;
    serde_yaml::from_str(&content)
        .map_err(|err| anyhow!("{}", err))
        .context("Failed to load database")
}

pub fn delete_node(config: &Config, node_name: String) -> Result<()> {
    let mut database = get_database(config)?;
    database.nodes.retain(|n| n.hostname != node_name);
    write_database(config, database)?;
    Ok(())
}

fn role_to_class(config: &Config, role: String) -> String {
    format!("{}{}", config.role.prefix, role)
}

pub fn modify_node(
    config: &Config,
    node_name: String,
    role: Option<String>,
    params: Option<HashMap<String, String>>,
) -> Result<()> {
    let mut database = get_database(config)?;
    let node = find_node(config, node_name).ok_or_else(|| anyhow!("Node not found"))?;
    let mut new_node = node.clone();

    // Check if role is valid if present
    if let Some(role) = &role {
        if !is_role_valid(config, role.to_string()) {
            bail!("Role '{}' is not valid", role);
        } else {
            new_node.classes = vec![role_to_class(config, role.clone())];
        }
    }

    if let Some(params) = &params {
        if let Some(new_node_parameters) = new_node.parameters.clone() {
            parameters_validation(
                config,
                &new_node_parameters
                    .into_iter()
                    .chain(params.clone())
                    .collect(),
            )?;
        }
        if !params.is_empty() {
            if new_node.parameters.is_none() {
                new_node.parameters = Some(HashMap::new());
            }
            for (key, value) in params {
                if value.is_empty() {
                    new_node.parameters.as_mut().unwrap().remove(key);
                    continue;
                }
                new_node
                    .parameters
                    .as_mut()
                    .unwrap()
                    .insert(key.clone(), value.clone());
            }
        }
    }

    database.nodes.retain(|n| n.hostname != node.hostname);
    database.nodes.push(new_node);
    write_database(config, database)?;
    Ok(())
}

pub fn get_all_nodes(config: &Config, filter_pattern: Option<String>) -> Result<Vec<Node>> {
    let database = get_database(config)?;

    // filter nodes if a pattern is provided
    if let Some(filter_pattern) = filter_pattern {
        let re = Regex::new(&filter_pattern).context("Failed to compile regex")?;
        // filter all fields of the node
        let nodes = database
            .nodes
            .into_iter()
            .filter(|node| {
                re.is_match(&node.hostname)
                    || re.is_match(&node.classes.join(" "))
                    || re.is_match(
                        &node
                            .parameters
                            .as_ref()
                            .unwrap_or(&HashMap::new())
                            .values()
                            .flat_map(|s| s.chars())
                            .collect::<String>(),
                    )
            })
            .collect();
        return Ok(nodes);
    }

    Ok(database.nodes)
}
