use std::{
    collections::HashMap,
    path::{Path, PathBuf},
};

use clap::command;
use enc_ng_lib::{Config, ParameterEntry};
use pretty_assertions::assert_eq;

// test for parse_key_val() in enc_ng_lib/src/lib.rs
#[test]
fn parse_key_val() {
    test_parse_key_val("key=val", Some("key"), Some("val"));
    test_parse_key_val("key=val val", Some("key"), Some("val val"));
    test_parse_key_val("key key=val", Some("key key"), Some("val"));
    test_parse_key_val("keyval", None, None);
    test_parse_key_val("12=34", Some("12"), Some("34"));
}

fn test_parse_key_val(key_val: &str, expected_key: Option<&str>, expected_val: Option<&str>) {
    use enc_ng_lib::parse_key_val;
    if expected_key.is_none() || expected_val.is_none() {
        assert!(parse_key_val::<String, String>(&key_val).is_err());
        return;
    }
    let (key, val): (String, String) = parse_key_val(&key_val).unwrap();
    assert_eq!(key, expected_key.unwrap());
    assert_eq!(val, expected_val.unwrap());
}

// test for add_default_arguments() in enc_ng_lib/src/lib.rs
#[test]
fn add_default_arguments() {
    test_add_default_arguments(None, Path::new("/etc/enc-ng/core.toml"));
    test_add_default_arguments(Some("/path/to/config"), Path::new("/path/to/config"));
    test_add_default_arguments(Some("config"), Path::new("config"));
    test_add_default_arguments(Some("../config"), Path::new("../config"));
}

fn test_add_default_arguments(path: Option<&str>, expected_path: &Path) {
    use enc_ng_lib::add_default_arguments;
    let mut cmd = command!();
    cmd = add_default_arguments(cmd);
    let mut args = vec!["nccli"];
    if let Some(path) = path {
        args.push("--config");
        args.push(path);
    }
    let matches = cmd.try_get_matches_from_mut(args).unwrap();
    assert_eq!(
        matches.get_one::<PathBuf>("config").unwrap(),
        &expected_path
    );
}

// test for read_core_config() in enc_ng_lib/src/lib.rs
#[test]
fn read_core_config() {
    use enc_ng_lib::Config;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    // test with a non-existent file
    test_read_core_config(Path::new("config"), None);

    // create a file with no permission to read
    let mut path = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path.push("core_no_perm.toml");
    std::fs::write(&path, "test").unwrap();
    std::fs::set_permissions(&path, std::os::unix::fs::PermissionsExt::from_mode(0o222)).unwrap();
    test_read_core_config(&path, None);

    // create a file with invalid content
    let mut path = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path.push("core_invalid.toml");
    std::fs::write(&path, "test").unwrap();
    test_read_core_config(&path, None);

    // create a file with default content
    let mut path = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path.push("core_good.toml");
    let mut content = String::new();
    content.push_str("[database]\n");
    content.push_str("backend_file = \"/tmp/enc-ng.yaml\"\n");
    content.push_str("[role]\n");
    content.push_str("prefix = \"role::\"\n");
    content.push_str("available = [ \"web\",\"dns\" ]\n");
    content.push_str("[environment]\n");
    content.push_str("default = \"production\"\n");
    content.push_str("[[parameters]]\n");
    content.push_str("name = \"comment\"\n");
    std::fs::write(&path, content).unwrap();

    let expected_config = Config {
        database: DatabaseConfig {
            backend_file: PathBuf::from("/tmp/enc-ng.yaml"),
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec!["web".to_string(), "dns".to_string()]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig {
            default: Some("production".to_string()),
        },
        parameters: vec![ParameterEntry {
            name: "comment".to_string(),
            required: None,
            allowed_values: None,
            format: None,
            description: None,
        }],
    };
    test_read_core_config(&path, Some(expected_config));
}

fn test_read_core_config(path: &Path, expected: Option<Config>) {
    use enc_ng_lib::read_core_config;
    let config = read_core_config(path);
    if expected.is_none() {
        assert!(config.is_err());
        return;
    }
    assert_eq!(config.unwrap().as_ref(), &expected.unwrap());
}

// test for add_node() in enc_ng_lib/src/lib.rs
#[test]
fn add_node() {
    use enc_ng_lib::find_node;
    use enc_ng_lib::Config;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    // add a node with backend_file absent
    let config = Config {
        database: DatabaseConfig {
            backend_file: PathBuf::from("/tmp/enc-ng.yaml"),
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec!["web".to_string(), "dns".to_string()]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![],
    };
    let new_node = enc_ng_lib::add_node(
        &config,
        "node-a.example.com".to_string(),
        "web".to_string(),
        None,
    );
    assert_eq!(new_node.is_err(), true);

    // add a node with backend_file invalid
    let mut path_backend_invalid = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend_invalid.push("backend_invalid.yaml");
    std::fs::write(&path_backend_invalid, "test").unwrap();
    let config = Config {
        database: DatabaseConfig {
            backend_file: path_backend_invalid,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec!["web".to_string(), "dns".to_string()]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![],
    };
    let new_node = enc_ng_lib::add_node(
        &config,
        "node-a.example.com".to_string(),
        "web".to_string(),
        None,
    );
    assert_eq!(new_node.is_err(), true);

    // add a node with backend_file valid
    let mut path_backend_valid = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend_valid.push("backend_valid.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend_valid, content).unwrap();
    let config = Config {
        database: DatabaseConfig {
            backend_file: path_backend_valid,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec!["web".to_string(), "dns".to_string()]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![],
    };
    let hostname = "node-a.example.com".to_string();
    let new_node = enc_ng_lib::add_node(&config, hostname.clone(), "web".to_string(), None);
    assert_eq!(new_node.unwrap(), ());
    let db_node = find_node(&config, hostname.clone());
    assert!(db_node.is_some());
    assert!(db_node
        .clone()
        .unwrap()
        .classes
        .contains(&"role::web".to_string()));
    assert_eq!(db_node.clone().unwrap().hostname, hostname);
    assert_eq!(db_node.clone().unwrap().parameters, None);

    // add a node with backend_file valid role invalid
    let hostname = "node-b.example.com".to_string();
    let new_node =
        enc_ng_lib::add_node(&config, hostname.clone(), "no_valid_role".to_string(), None);
    assert!(new_node.is_err());
    let db_node = find_node(&config, hostname.clone());
    assert!(db_node.is_none());

    // add a node and this node already exists
    let hostname = "node-a.example.com".to_string();
    let new_node = enc_ng_lib::add_node(&config, hostname.clone(), "web".to_string(), None);
    assert!(new_node.is_err());

    // add a node with no verification of role
    let mut path_backend_check_role = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend_check_role.push("backend_check_role.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend_check_role, content).unwrap();
    let config_no_available_roles = Config {
        database: DatabaseConfig {
            backend_file: path_backend_check_role,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![],
    };
    let hostname = "node-c.example.com".to_string();
    let new_node = enc_ng_lib::add_node(
        &config_no_available_roles,
        hostname.clone(),
        "no_valid_role".to_string(),
        None,
    );
    assert_eq!(new_node.unwrap(), ());
    let db_node = find_node(&config_no_available_roles, hostname.clone());
    assert!(db_node.is_some());
    assert!(db_node
        .clone()
        .unwrap()
        .classes
        .contains(&"role::no_valid_role".to_string()));

    // add a node with parameters
    let mut path_backend_parameters = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend_parameters.push("backend_parameters.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend_parameters, content).unwrap();
    let config_parameters = Config {
        database: DatabaseConfig {
            backend_file: path_backend_parameters,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec!["web".to_string(), "dns".to_string()]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![ParameterEntry {
            name: "comment".to_string(),
            required: Some(false),
            allowed_values: None,
            format: None,
            description: None,
        }],
    };
    let hostname = "node-d.example.com".to_string();
    let new_node = enc_ng_lib::add_node(
        &config_parameters,
        hostname.clone(),
        "web".to_string(),
        Some(HashMap::from([(
            "comment".to_string(),
            "This is a comment".to_string(),
        )])),
    );
    assert_eq!(new_node.unwrap(), ());
    let db_node = find_node(&config_parameters, hostname.clone());
    assert!(db_node.is_some());
    assert!(db_node
        .clone()
        .unwrap()
        .classes
        .contains(&"role::web".to_string()));
    assert_eq!(db_node.clone().unwrap().hostname, hostname);
    assert_eq!(
        db_node
            .clone()
            .unwrap()
            .parameters
            .unwrap()
            .get("comment")
            .unwrap(),
        &"This is a comment".to_string()
    );
}

// test for parameters_validation() in enc_ng_lib/src/lib.rs
#[test]
fn parameters_validation() {
    use enc_ng_lib::parameters_validation;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    // configuration
    let config = Config {
        database: DatabaseConfig {
            backend_file: PathBuf::from("/tmp/enc-ng.yaml"),
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![
            ParameterEntry {
                name: "comment".to_string(),
                required: Some(false),
                allowed_values: None,
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "comment_required".to_string(),
                required: Some(true),
                allowed_values: None,
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "check_regex".to_string(),
                required: Some(false),
                allowed_values: None,
                format: Some("^[0-9]*$".to_string()),
                description: None,
            },
            ParameterEntry {
                name: "check_value".to_string(),
                required: Some(false),
                allowed_values: Some(vec!["value1".to_string(), "value2".to_string()]),
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "check_value_and_regex".to_string(),
                required: Some(false),
                allowed_values: Some(vec!["value1".to_string(), "value2".to_string()]),
                format: Some("^[0-9]{4}$".to_string()),
                description: None,
            },
        ],
    };

    // unknown parameter
    let parameters = HashMap::from([
        ("unknown".to_string(), "test".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    // comment with free text
    let parameters = HashMap::from([
        ("comment".to_string(), "This is a comment".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert_eq!(result.unwrap(), ());

    // comment_required is required
    let parameters = HashMap::from([("param".to_string(), "value".to_string())]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    // check value with available values
    let parameters = HashMap::from([
        ("check_value".to_string(), "value".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    let parameters = HashMap::from([
        ("check_value".to_string(), "value2".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert_eq!(result.unwrap(), ());

    // check value with regex
    let parameters = HashMap::from([
        ("check_regex".to_string(), "value".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    let parameters = HashMap::from([
        ("check_regex".to_string(), "0123456789".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert_eq!(result.unwrap(), ());

    // check value with available values and regex
    let parameters = HashMap::from([
        ("check_value_and_regex".to_string(), "value1".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    let parameters = HashMap::from([
        ("check_value_and_regex".to_string(), "example".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert!(result.is_err());

    let parameters = HashMap::from([
        ("check_value_and_regex".to_string(), "1234".to_string()),
        ("comment_required".to_string(), "value".to_string()),
    ]);
    let result = parameters_validation(&config, &parameters);
    assert_eq!(result.unwrap(), ());
}

// test for get_enc_ng_config() in enc_ng_lib/src/lib.rs
#[test]
fn get_enc_ng_config() {
    use enc_ng_lib::add_node;
    use enc_ng_lib::get_enc_for_puppet;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_get_enc_puppet.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();

    let config_v1 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: Some(true),
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![
            ParameterEntry {
                name: "comment".to_string(),
                required: Some(false),
                allowed_values: None,
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "check_value".to_string(),
                required: Some(false),
                allowed_values: Some(vec!["value1".to_string(), "value2".to_string()]),
                format: None,
                description: None,
            },
        ],
    };

    // add a node without parameters
    add_node(&config_v1, "node1".to_string(), "test".to_string(), None).unwrap();
    let enc = get_enc_for_puppet(&config_v1, "node1".to_string());
    assert!(enc.is_ok());
    assert_eq!(
        enc.unwrap(),
        "---\nclasses:\n  - role::test\nparameters:\n  role: role::test"
    );

    // add a node with parameters
    add_node(
        &config_v1,
        "node2".to_string(),
        "web".to_string(),
        Some(HashMap::from([
            ("comment".to_string(), "this is a comment".to_string()),
            ("check_value".to_string(), "value1".to_string()),
        ])),
    )
    .unwrap();
    let enc = get_enc_for_puppet(&config_v1, "node2".to_string());
    assert!(enc.is_ok());
    assert_eq!(
        enc.unwrap(),
        "---\nclasses:\n  - role::web\nparameters:\n  check_value: value1\n  comment: this is a comment\n  role: role::web"
    );

    // try with environment
    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_get_enc_puppet.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();
    let config_v2 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: Some(true),
            set_prefix_in_parameter: Some(false),
        },
        environment: EnvironmentConfig {
            default: Some("env_test".to_string()),
        },
        parameters: vec![],
    };
    add_node(
        &config_v2,
        "node-test".to_string(),
        "proxy".to_string(),
        None,
    )
    .unwrap();
    let enc = get_enc_for_puppet(&config_v2, "node-test".to_string());
    assert!(enc.is_ok());
    assert_eq!(
        enc.unwrap(),
        "---\nclasses:\n  - role::proxy\nparameters:\n  role: proxy\nenvironment: env_test"
    );
}

// test for delete_node() in enc_ng_lib/src/lib.rs
#[test]
fn delete_node() {
    use enc_ng_lib::add_node;
    use enc_ng_lib::delete_node;
    use enc_ng_lib::find_node;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::RoleConfig;

    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_delete_node.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();

    let config_v1 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![],
    };

    // delete a node that does not exist
    let res = delete_node(&config_v1, "node1".to_string());
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), ());

    // add a node without parameters
    add_node(&config_v1, "node1".to_string(), "test".to_string(), None).unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_some());

    // delete the node
    delete_node(&config_v1, "node1".to_string()).unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_none());
}

// test for find_node() in enc_ng_lib/src/lib.rs
#[test]
fn find_node() {
    use enc_ng_lib::add_node;
    use enc_ng_lib::find_node;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::Node;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_find_node.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();

    let config_v1 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: None,
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![
            ParameterEntry {
                name: "comment".to_string(),
                required: Some(false),
                allowed_values: None,
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "check_value".to_string(),
                required: Some(false),
                allowed_values: Some(vec!["value1".to_string(), "value2".to_string()]),
                format: None,
                description: None,
            },
        ],
    };

    // find a node that does not exist
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_none());

    // add a node without parameters
    add_node(&config_v1, "node1".to_string(), "test".to_string(), None).unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_some());
    assert_eq!(
        node.unwrap(),
        Node {
            hostname: "node1".to_string(),
            classes: vec!["role::test".to_string()],
            parameters: None,
        }
    );

    // add a node with parameters
    add_node(
        &config_v1,
        "node2".to_string(),
        "web".to_string(),
        Some(HashMap::from([
            ("comment".to_string(), "this is a comment".to_string()),
            ("check_value".to_string(), "value1".to_string()),
        ])),
    )
    .unwrap();
    let node = find_node(&config_v1, "node2".to_string());
    assert!(node.is_some());
    assert_eq!(
        node.unwrap(),
        Node {
            hostname: "node2".to_string(),
            classes: vec!["role::web".to_string()],
            parameters: Some(HashMap::from([
                ("comment".to_string(), "this is a comment".to_string()),
                ("check_value".to_string(), "value1".to_string()),
            ])),
        }
    );
}

// test for modify_node() in enc_ng_lib/src/lib.rs
#[test]
fn modify_node() {
    use enc_ng_lib::add_node;
    use enc_ng_lib::find_node;
    use enc_ng_lib::modify_node;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::Node;
    use enc_ng_lib::ParameterEntry;
    use enc_ng_lib::RoleConfig;

    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_modify_node.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();

    let config_v1 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec![
                "test".to_string(),
                "web".to_string(),
                "db".to_string(),
                "app".to_string(),
            ]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![
            ParameterEntry {
                name: "comment".to_string(),
                required: Some(false),
                allowed_values: None,
                format: None,
                description: None,
            },
            ParameterEntry {
                name: "check_value".to_string(),
                required: Some(false),
                allowed_values: Some(vec!["value1".to_string(), "value2".to_string()]),
                format: None,
                description: None,
            },
        ],
    };

    // modify a node that does not exist
    let res = modify_node(
        &config_v1,
        "node1".to_string(),
        Some("test".to_string()),
        None,
    );
    assert!(res.is_err());
    assert_eq!(res.unwrap_err().to_string(), "Node not found");

    // add a node without parameters
    add_node(&config_v1, "node1".to_string(), "test".to_string(), None).unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_some());
    assert_eq!(
        node.unwrap(),
        Node {
            hostname: "node1".to_string(),
            classes: vec!["role::test".to_string()],
            parameters: None,
        }
    );

    // modify the node
    modify_node(
        &config_v1,
        "node1".to_string(),
        Some("web".to_string()),
        Some(HashMap::from([
            ("comment".to_string(), "this is a comment".to_string()),
            ("check_value".to_string(), "value1".to_string()),
        ])),
    )
    .unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_some());
    assert_eq!(
        node.unwrap(),
        Node {
            hostname: "node1".to_string(),
            classes: vec!["role::web".to_string()],
            parameters: Some(HashMap::from([
                ("comment".to_string(), "this is a comment".to_string()),
                ("check_value".to_string(), "value1".to_string()),
            ])),
        }
    );

    // remove comment parameter
    modify_node(
        &config_v1,
        "node1".to_string(),
        None,
        Some(HashMap::from([("comment".to_string(), "".to_string())])),
    )
    .unwrap();
    let node = find_node(&config_v1, "node1".to_string());
    assert!(node.is_some());
    assert_eq!(
        node.unwrap(),
        Node {
            hostname: "node1".to_string(),
            classes: vec!["role::web".to_string()],
            parameters: Some(HashMap::from([(
                "check_value".to_string(),
                "value1".to_string()
            ),])),
        }
    );

    // set a invalid role
    let res = modify_node(
        &config_v1,
        "node1".to_string(),
        Some("invalid".to_string()),
        None,
    );
    assert!(res.is_err());
}

// test for get_all_nodes() in enc_ng_lib/src/lib.rs
#[test]
fn get_all_nodes() {
    use enc_ng_lib::add_node;
    use enc_ng_lib::get_all_nodes;
    use enc_ng_lib::DatabaseConfig;
    use enc_ng_lib::EnvironmentConfig;
    use enc_ng_lib::Node;
    use enc_ng_lib::RoleConfig;

    let mut path_backend = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    path_backend.push("backend_get_all_nodes.yaml");
    let mut content = String::new();
    content.push_str("version: '1.0'\n");
    content.push_str("nodes: []\n");
    std::fs::write(&path_backend, content).unwrap();

    let config_v1 = Config {
        database: DatabaseConfig {
            backend_file: path_backend,
        },
        role: RoleConfig {
            prefix: "role::".to_string(),
            available: Some(vec![
                "test".to_string(),
                "web".to_string(),
                "db".to_string(),
                "app".to_string(),
            ]),
            add_role_in_parameters: None,
            set_prefix_in_parameter: None,
        },
        environment: EnvironmentConfig { default: None },
        parameters: vec![ParameterEntry {
            name: "comment".to_string(),
            required: Some(false),
            allowed_values: None,
            format: None,
            description: None,
        }],
    };

    // get all nodes from an empty database
    let nodes = get_all_nodes(&config_v1, None);
    assert!(nodes.is_ok());
    assert_eq!(nodes.unwrap(), vec![]);

    // add a node
    add_node(&config_v1, "node1".to_string(), "test".to_string(), None).unwrap();
    let nodes = get_all_nodes(&config_v1, None);
    assert!(nodes.is_ok());
    assert_eq!(
        nodes.unwrap(),
        vec![Node {
            hostname: "node1".to_string(),
            classes: vec!["role::test".to_string()],
            parameters: None,
        }]
    );

    // add a second node
    add_node(
        &config_v1,
        "node2".to_string(),
        "web".to_string(),
        Some(HashMap::from([(
            "comment".to_string(),
            "this is a comment".to_string(),
        )])),
    )
    .unwrap();
    let nodes = get_all_nodes(&config_v1, None);
    assert!(nodes.is_ok());
    assert_eq!(
        nodes.unwrap(),
        vec![
            Node {
                hostname: "node1".to_string(),
                classes: vec!["role::test".to_string()],
                parameters: None,
            },
            Node {
                hostname: "node2".to_string(),
                classes: vec!["role::web".to_string()],
                parameters: Some(HashMap::from([(
                    "comment".to_string(),
                    "this is a comment".to_string()
                ),])),
            }
        ]
    );

    // search node with hostname
    let nodes = get_all_nodes(&config_v1, Some("node1".to_string()));
    assert!(nodes.is_ok());
    assert_eq!(
        nodes.unwrap(),
        vec![Node {
            hostname: "node1".to_string(),
            classes: vec!["role::test".to_string()],
            parameters: None,
        }]
    );

    // search node with role
    let nodes = get_all_nodes(&config_v1, Some("web".to_string()));
    assert!(nodes.is_ok());
    assert_eq!(
        nodes.unwrap(),
        vec![Node {
            hostname: "node2".to_string(),
            classes: vec!["role::web".to_string()],
            parameters: Some(HashMap::from([(
                "comment".to_string(),
                "this is a comment".to_string()
            ),])),
        }]
    );

    // search node with parameter
    let nodes = get_all_nodes(&config_v1, Some("this".to_string()));
    assert!(nodes.is_ok());
    assert_eq!(
        nodes.unwrap(),
        vec![Node {
            hostname: "node2".to_string(),
            classes: vec!["role::web".to_string()],
            parameters: Some(HashMap::from([(
                "comment".to_string(),
                "this is a comment".to_string()
            ),])),
        }]
    );

    // search node with not match
    let nodes = get_all_nodes(&config_v1, Some("not_match".to_string()));
    assert!(nodes.is_ok());
    assert_eq!(nodes.unwrap(), vec![]);
}
