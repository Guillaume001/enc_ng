image: rust:slim

stages:
  - build
  - test
  - staging

.build_project: &build_project |
  cargo build --release

.install_outdated: &install_outdated |
  cargo install cargo-outdated

.setup_dependencies: &setup_dependencies
  before_script:
    - apt update
    - apt install libssl-dev librust-openssl-sys-dev -y

.setup_packaging: &setup_packaging
  - curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path
  - source "$HOME/.cargo/env"
  - dnf install openssl-devel gcc -y
  - cargo install cargo-generate-rpm

.build_rpm: &build_rpm
  - cargo generate-rpm -p enc_ng
  - mv target/generate-rpm/*.rpm .

.use_cache_build: &use_cache_build
  cache:
    key: target_build
    paths:
      - "target"
    policy: pull


build:
  stage: build
  <<: *setup_dependencies
  script:
    - *build_project
  cache:
    key: target_build
    paths:
      - "target"
    policy: push
  artifacts:
    paths:
      - target/release/nccli
      - target/release/enc_puppet

cargo-test:
  stage: test
  tags:
      - privileged
  coverage: '/^\d+.\d+% coverage/'
  needs:
    - build
  before_script:
    - apt update
    - apt install -y pkg-config libssl-dev
    - cargo install cargo-tarpaulin
  script:
    - cargo tarpaulin --out xml --all-features --output-dir target/
  <<: *use_cache_build
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/cobertura.xml

lint-clippy:
  stage: test
  needs:
    - build
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy -- -Dwarnings
  <<: *use_cache_build

lint-fmt:
  stage: test
  needs:
    - build
  before_script:
    - rustup component add rustfmt
  script:
    - cargo fmt -- --check
  <<: *use_cache_build

check-outdated-project:
  stage: test
  needs:
    - build
  <<: *setup_dependencies
  script:
    - *install_outdated
    - cargo outdated --exit-code 1 --root-deps-only
  <<: *use_cache_build

check-outdated-all:
  stage: test
  needs:
    - build
  <<: *setup_dependencies
  script:
    - *install_outdated
    - cargo outdated --exit-code 1
  allow_failure: true
  <<: *use_cache_build

check-audit:
  stage: test
  needs:
    - build
  <<: *setup_dependencies
  script:
    - cargo install cargo-audit
    - cargo audit
  <<: *use_cache_build

doc:
  stage: staging
  script:
    - cargo doc --release --no-deps
  <<: *use_cache_build
  artifacts:
    paths:
      - target/doc

build-rhel:
  stage: staging
  image: almalinux:${RHEL_VERSION}
  script:
    - *setup_packaging
    - cargo test --release
    - cargo build --release
    - *build_rpm
  artifacts:
    paths:
      - "*.rpm"
  only:
    - main
  parallel:
    matrix:
      - RHEL_VERSION:
        - 8
        - 9
